﻿//Autor: Cezary Wernik
//Opis: Wstęp do pisania wstawek asemblera w języku C/C++

#include <iostream>
#include <bitset>
using namespace std;

//funkcje pomocnicze 
string itbs(int value) //Int To Bin String
{
    return bitset<32>(value).to_string();
}

void showVariable(int *var, string name)
{
    cout << "[" << var << "] : " << itbs(*var) << " BIN = " << *var << " DEC ("<<name<<")"<<endl;
}

//funkcje pomocnicze użyte w przykładach
void procedura()
{
    //nie robię nic :)
}

int funkcja() 
{
    //zwracam 5
    return 5;
}

int suma(int p, int q, int r) 
{
    //zwracam sumę a+b+c
    return p + q + r;
}

int main()
{
    /*
        ___________________________________________________________________________________________

                                        ...::: Krótki wstęp teoretyczny :::...
        ___________________________________________________________________________________________

        0) Wstawki asemblerowe w języku C/C++ dla Visual Studio

            Składnia:

                __asm {

                }

            Komentarze w kodzie wstawki rozpoczynają się zawsze od znaku średnika ";" np.:

                __asm {
                    ...
                    mov eax, p ; tu jest komentarz do tej linii kodu rozpoczęty średnikiem
                    ...
                    ...
                    mov [wynik], eax
                    ...
                }

        1) Rejestry w architekturze x86

            1.1) Rejestry ogólnego przeznaczenia

                    |<------------- 32 bity ----------->|
                eax [-------- ------ax ------ah ------al]
                ebx [-------- ------bx ------bh ------bl]
                ecx [-------- ------cx ------ch ------cl]
                edx [-------- ------dx ------dh ------dl]
                esi [-------- -------- -------- --------]
                edi [-------- -------- -------- --------]
                esp [-------- -------- -------- --------]
                ebp [-------- -------- -------- --------]

                Opisy:
                    eax - używany głównie w czasie operacji arytmetycznych
                    ebx - rejestr bazowy, wraz z rejestrem segmentowym DS tworzą wskaźnik 
                          na przestrzeń danych
                    ecx - rejestr służący głownie jako licznik w zapętlonych operacjach
                    edx - rejestr danych, używany do operacji arytmetycznych oraz obsługi 
                          wejścia-wyjścia
                    esp - wskaźnik wierzchołka stosu
                    ebp - wskaźnik do danych w segmencie stosu
                    esi - rejestr indeksowy, wskaźnik źródła
                    edi - rejestr indeksowy, wskaźnik przeznaczenia

            1.2) Rejestr flagowy:

                31  30  29  28  27  26  25  24  23  22  21  20  19  18  17  16
                0   0   0   0   0   0   0   0   0   0   ID  VIP VIF AC  VM  RF

                15  14  13  12  11  10  9   8   7   6   5   4   3   2   1   0
                0   NT  <IOPL>  OF  DF  IF  TF  SF  ZF  0   AF  0   PF  1   CF

                Flagi używane przy operacjach arytmetycznych:
                    CF - carry flag - if 1 then - w wyniku dodawania/odejmowania przekroczono
                         możliwy zakres wartości zmiennej
                    PF - parity flag - if 1 then - liczba jedynek w najmłodszym bajcie jest
                         parzysta
                    AF - auxiliary flag - if 1 then - wystąpiło przeniesienie z bitu 3. na 4.
                         lub pożyczka z 4. na 3.
                    ZF - zero flag - if 1 then - wynik ostatniego działania wyniósł 0
                    SF - sign flag - if 1 then - bit znaku jest równy 1 (liczba jest ujemna)
                    OF - overflow flag - if 1 then - bit znaku zmienił swoją wartość, ale nie
                         doszło do przeniesienia poza zakres (tzn. CF=0)

                Pozostałe:
                    NT  - nested task - if 1 then - obecny proces nie wywodzi się z żadnego 
                          innego (nie wywołano go poprzez CALL; nie dotyczy JMP)
                    RF  - resume flag - if 1 then - wystąpiło przerwanie zatrzymujące wykonanie
                          programu (może wystąpić tylko, gdy TF=1)
                    VIP - virtual interrupt pending - if 1 then - trwa wirtualne przerwanie
                    ID  - ID flag - if 1 then - jeśli można zmienić jej wartość oznacza to, 
                          że procesor obsługuje instrukcję CPUID
                    TF  - trap flag - if 1 then - procesor wchodzi w tryb pracy krokowej, 
                          dzięki czemu po każdym wykonanym poleceniu generuje przerwanie i
                          przechodzi do odpowiednich procedur obsługi 
                          (wykorzystywane przez np. debuggery)
                    IF  - interrupt flag - if 1 then - przerwania są uwzględniane 
                          (w przeciwnym wypadku są ignorowane)
                    DF  - direction flag - if 1 then - przetwarzanie łańcuchów odbywa się w
                          normalny sposób, ku rosnącym adresom; w przeciwnym wypadku w dół,
                          ku malejącym
                    IOPL - input-output privilage level - if 1 then - odczyt/zapis z/do 
                          wyjścia jest dozwolony; jest to 2-bitowa flaga
                    VM  - virtual 8086 mode - if 1 then - przejście w tryb emulacji procesora 8086
                    VIF - virtual interrupt flag - if 1 then - przerwania są dozwolone (kopia IF w
                          trybie wirtualnym); aby tryb wirtualny został aktywowany musi zostać
                          włączona flaga VME w rejestrze CR4

        2) Podstawowe instrukcje asemblerowe

            Używana notacja:
                <rej32> - rejestr np.: eax, ebx, ecx, edx, esi, edi, esp, ebp
                <rej16> - rejestr np.: ax, bx, cx, dx
                <rej8>  - rejestr np.: ah, al, bh, bl, ch, cl, dh, dl
                <rej>   - wskazanie na rejestr
                <pam>   - wskazanie na adres pamięci np.:[eax], [eax+ebx]
                <sta32> - stała 32-bitowa
                <sta16> - stała 16-bitowa
                <sta8>  - stała 8-bitowa
                <sta>   - stała 8-,16-,32-bitowa
                <etykieta> - etykieta/adres

            Przyjęta główna konwencja przepisywania danych dla stdcall:

                instrukcja cel, źródło

                np.:
                mov eax, <sta>

                gdzie <sta> to zdefiniowana wcześniej zmienna lub wartość liczbowa

            2.1) Instrukcje przemieszczania danych/adresów

                mov - przesunięcie
                Składnia:
                    mov <rej>, <rej>
                    mov <rej>, <pam>
                    mov <pam>, <rej>
                    mov <rej>, <sta>
                    mov <pam>, <sta>

                push - odłożenie na stosie
                Składnia:
                    push <rej32>
                    push <pam>
                    push <sta32>

                pop - zdjęcie ze stosu
                Składnia:
                    pop <rej32>
                    pop <pam>

                lea - wczytanie adresu
                Składnia:
                    lea <rej32>, <pam>

            2.2) Instrukcje arytmetyczne

                add - dodawanie
                Składnia:
                    add <rej>, <rej>
                    add <rej>, <pam>
                    add <pam>, <rej>
                    add <rej>, <sta>
                    add <pam>, <sta>

                sub - odejmowanie
                Składnia:
                    sub <rej>, <rej>
                    sub <rej>, <pam>
                    sub <pam>, <rej>
                    sub <rej>, <sta>
                    sub <pam>, <sta>

                inc - zwiększenie o jeden
                Składnia:
                    inc <rej>
                    inc <pam>

                dec - pomniejszenie o jeden
                Składnia:
                    dec <rej>
                    dec <pam>

                imul - mnożenie
                Składnia:
                    imul <rej32>, <rej32>
                    imul <rej32>, <pam>
                    imul <rej32>, <rej32>, <sta>
                    imul <rej32>, <pam>, <sta>

                idiv - dzielenie
                Składnia:
                    idiv <rej32>
                    idiv <pam>

            2.3) Instrukcje logiczne

                and - koniunkcja "i"
                Składnia:
                    and <rej>, <rej>
                    and <rej>, <pam>
                    and <pam>, <rej>
                    and <rej>, <sta>
                    and <pam>, <sta>

                or - alternatywa "lub"
                Składnia:
                    or <rej>, <rej>
                    or <rej>, <pam>
                    or <pam>, <rej>
                    or <rej>, <sta>
                    or <pam>, <sta>

                xor - alternatywa wykluczająca
                Składnia:
                    xor <rej>, <rej>
                    xor <rej>, <pam>
                    xor <pam>, <rej>
                    xor <rej>, <sta>
                    xor <pam>, <sta>

                not - zaprzeczenie
                Składnia:
                    not <rej>
                    not <pam>

                neg - negacja
                Składnia:
                    neg <rej>
                    neg <pam>

                shl - przesunięcie bitowe w lewo
                Składnia:
                    shl <rej>, <sta8>
                    shl <pam>, <sta8>

                shr - przesunięcie bitowe w prawo
                Składnia:
                    shr <rej>, <sta8>
                    shr <pam>, <sta8>

            2.4) Instrukcje kontroli wykonania

                cmp - porównanie
                Składnia:
                    cmp <rej>, <rej>
                    cmp <rej>, <pam>
                    cmp <pam>, <rej>
                    cmp <rej>, <sta>

                jmp - skok bezwarunkowy
                Składnia:
                    jmp <etykieta>

                Instrukcje skoku warunkowego -
                Składnia:
                    je  <etykieta> ;skocz gdy równe
                    jnz <etykieta> ;skocz gdy nierówne
                    jz  <etykieta> ;skocz gdy wynik ostatniej operacji równy zero
                    jg  <etykieta> ;skocz gdy większe
                    jge <etykieta> ;skocz gdy większe lub równe
                    jl  <etykieta> ;skocz gdy mniejsze
                    jle <etykieta> ;skocz gdy mniejsze lub równe

                call - wywołaj podprogram (skok do adresu podprogramu)
                Składnia:
                    call <etykieta>

                ret - powrót z podprogramu
                Składnia:
                    ret

                nop - nie rób nic
                Składnia:
                    nop

        ___________________________________________________________________________________________

                                        ...::: Część praktyczna :::...
        ___________________________________________________________________________________________

    */

    //Zmienne pomocnicze
    int p = 715827882;
    int q = 1431655765;
    int r = 5;
    int wynik = 0;

    showVariable(&p, "p");
    showVariable(&q, "q");
    showVariable(&wynik, "wynik");

    //--- Przenoszenie zawartości zmiennych do rejestrów
    __asm {
        mov eax, p ;zawartość zmiennej p została przepisana do rejestru eax
    }

    /*
        p =  715827882 DEC = 00101010 10101010 10101010 10101010 BIN

        Podgląd na rejestry:
                | <------------- 32 bits ---------> |
            eax [00101010 10101010 10101010 10101010]
            ebx [???????? ???????? ???????? ????????]
            ...
    */

    //--- Przenoszenie zawartości zmiennych do rejestrów cd..
    __asm {
        mov eax, p ;zawartość zmiennej p została przepisana do rejestru eax
        mov ebx, q ;zawartość zmiennej q została przepisana do rejestru ebx
    }

    /*
        p =  715827882 DEC => 00101010 10101010 10101010 10101010 BIN
        q = 1431655765 DEC => 01010101 01010101 01010101 01010101 BIN

        Podgląd na rejestry:
                | <------------- 32 bits ---------> |
            eax [00101010 10101010 10101010 10101010]
            ebx [01010101 01010101 01010101 01010101]
            ...
    */

    //--- Przenoszenie zawartości rejestrów do zmiennych
    __asm {
        mov eax, p
        mov ebx, q
        mov [wynik], eax ;pod adres[wynik] została przepisana zawartość rejestru eax
    }
    showVariable(&wynik, "wynik = p");

    //--- Dodawanie zawartości rejestrów 
    __asm {
        mov eax, p
        mov ebx, q
        add eax, ebx ;zawartość rejestrów eax i ebx została do siebie dodana,
        ;wynik został zapisany do rejestru eax
        mov [wynik], eax
    }
    showVariable(&wynik, "wynik = p + q");

    /*
        Zawartość rejestrów po wykonaniu instrukcji
        add eax, ebx
        spowoduje dodanie zawartości rejestrów eax i ebx zgodnie z regułami operacji arytmetycznych
        w systemie dwójkowym.

        p =  715827882 DEC => 00101010 10101010 10101010 10101010 BIN
        q = 1431655765 DEC => 01010101 01010101 01010101 01010101 BIN
                           + ________________________________________
                              01111111 11111111 11111111 11111111 BIN

        Podgląd na rejestry:
                | <------------- 32 bits ---------> |
            eax [01111111 11111111 11111111 11111111]
            ebx [01010101 01010101 01010101 01010101]
            ...

        Działania arytmetyczne na liczbach w systemie dwójkowym są szczegółowo omówione w książce
        Podstawy elektroniki cyfrowej - prof. dr hab. inż. Józef Kalisz - WKŁ
    */

    //--- Odejmowanie zawartości rejestrów
    __asm {
        mov eax, p
        mov ebx, q
        sub eax, ebx ;zawartość rejestru eax została pomniejszona o zawartość rejestru ebx,
        ;wynik został zapisany do rejestru eax
        mov [wynik], eax
    }
    showVariable(&wynik, "wynik = p - q");

    /*
        p =  715827882 DEC => 00101010 10101010 10101010 10101010 BIN
        q = 1431655765 DEC => 01010101 01010101 01010101 01010101 BIN
                           - ________________________________________
                              11010101 01010101 01010101 01010101 BIN

        Podgląd na rejestry:
                | <------------- 32 bits ---------> |
            eax [11010101 01010101 01010101 01010101]
            ebx [01010101 01010101 01010101 01010101]
            ...
    */

    //--- Inkrementacja wartości rejestru
    __asm {
        mov eax, p
        inc eax
        mov [wynik], eax
    }
    showVariable(&wynik, "wynik = p++");

    /*
        p =  715827882 DEC => 00101010 10101010 10101010 10101010 BIN

        p = p + 1 = 715827882 + 1 = 715827883
        715827883 DEC => 00101010101010101010101010101011 BIN

        Podgląd na rejestry:
                | <------------- 32 bits ---------> |
            eax [00101010 10101010 10101010 10101011]
            ebx [???????? ???????? ???????? ????????]
            ...
    */

    //--- Dekrementacja wartości rejestru
    __asm {
        mov eax, p
        dec eax
        mov [wynik], eax
    }
    showVariable(&wynik, "wynik = p--");

    /*
        p =  715827882 DEC => 00101010 10101010 10101010 10101010 BIN

        p = p - 1 = 715827882 - 1 = 715827881
        715827881 DEC => 00101010101010101010101010101001 BIN

        Podgląd na rejestry:
                | <------------- 32 bits ---------> |
            eax [00101010 10101010 10101010 10101001]
            ebx [???????? ???????? ???????? ????????]
            ...
    */

    //--- Dzielenie 
    int reszta = 0;
    __asm {
        mov eax, p ;do rejestru eax została przeniesiona wartość zmiennej p(licznik)
        mov edx, 0 ;rejestr edx został wyzerowany - w rejestrze edx zostanie przechowana
                   ;potencjalna reszta z dzielenia
        mov ecx, 2 ;do rejestru ecx została zapisana wartość 2 (mianownik)
        idiv ecx   ;zawartość rejestru eax została podzielona przez zawartość rejestru ecx
        mov [wynik], eax
        mov [reszta], edx
    }
    showVariable(&wynik, "wynik = p / 2");
    showVariable(&reszta, "reszta z p / 2");

    /*
        p =  715827882 DEC => 00101010 10101010 10101010 10101010 BIN

        p / 2 = 357913941 DEC => 00010101 01010101 01010101 01010101 BIN

        Podgląd na rejestry:
            | <------------- 32 bits ---------> |
        eax [00010101 01010101 01010101 01010101]
        ebx [???????? ???????? ???????? ????????]
        ecx [00000000 00000000 00000000 00000010]
        edx [00000000 00000000 00000000 00000000]
        ...
    */

    //--- Mnożenie 
    __asm {
        mov eax, wynik ;wykorzystana został tu wartość obliczona w poprzednim działaniu
                       ;(dzieleniu p / 2)
        imul eax, 2    ;zawartość rejestru eax została pomnożona przez 2
        mov [wynik], eax
    }
    showVariable(&wynik, "wynik = wynik * 2 = (p / 2) * 2 = p");

    /*
        Do rejestru eax została przepisana liczba która była obliczona w poprzednim przykładzie,
        ponieważ było to p/2, a w tym przykładzie zawartość rejestru eax jest mnożona przez 2 w
        wyniku powinna znaleźć się z powrotem wartość zmiennej p jaka była przed dzieleniem.
    */

    //--- Koniunkcja
    __asm {
        mov eax, p
        mov ebx, q
        and eax, ebx ;została wykonana bitowa operacja koniunkcji(and) pomiędzy rejestrami
                     ;eax i ebx
        mov [wynik], eax
    }
    showVariable(&wynik, "wynik = p&p");

    /*
        p =  715827882 DEC => 00101010 10101010 10101010 10101010 BIN
        q = 1431655765 DEC => 01010101 01010101 01010101 01010101 BIN
                           & ________________________________________
                              00000000 00000000 00000000 00000000 BIN

        Podgląd na rejestry:
                | <------------- 32 bits ---------> |
            eax [00000000 00000000 00000000 00000000]
            ebx [01010101 01010101 01010101 01010101]
            ...
    */

    //--- Alternatywa
    __asm {
        mov eax, p
        mov ebx, q
        or eax, ebx ;została wykonana bitowa operacja alternatywy(or ) pomiędzy rejestrami 
                    ;eax i ebx
        mov [wynik], eax
    }
    showVariable(&wynik, "wynik = p|p");

    /*
        p =  715827882 DEC => 00101010 10101010 10101010 10101010 BIN
        q = 1431655765 DEC => 01010101 01010101 01010101 01010101 BIN
                           | ________________________________________
                              01111111 11111111 11111111 11111111 BIN
        Podgląd na rejestry:
                | <------------- 32 bits ---------> |
            eax [01111111 11111111 11111111 11111111]
            ebx [01111111 11111111 11111111 11111111]
            ...
    */

    //--- Alternatywa wykluczająca
    __asm {
        mov eax, p
        mov ebx, q
        xor eax, ebx ;została wykonana bitowa operacja alternatywy wykluczającej(xor) pomiędzy
                     ;rejestrami eax i ebx
        mov [wynik], eax
    }
    showVariable(&wynik, "wynik = p^p");

    /*
        p =  715827882 DEC => 00101010 10101010 10101010 10101010 BIN
        q = 1431655765 DEC => 01010101 01010101 01010101 01010101 BIN
                           ^ ________________________________________
                              01111111 11111111 11111111 11111111 BIN

        Podgląd na rejestry:
                | <------------- 32 bits ---------> |
            eax [00010101 01010101 01010101 01010101]
            ebx [???????? ???????? ???????? ????????]
            ...
    */

    //--- Zaprzeczenie (uzupełnienie jedynkowe)
    __asm {
        mov eax, p
        not eax ;zawartość rejestru eax została zaprzeczona
        mov [wynik], eax

    }
    showVariable(&wynik, "wynik = ~p");

    /*
        Operacja zaprzeczenia odwraca wszystkie bity wskazanego rejestru.

        p =  715827882 DEC => 00101010 10101010 10101010 10101010 BIN
                            ~ _______________________________________
                              11010101 01010101 01010101 01010101 BIN

        Podgląd na rejestry:
                | <------------- 32 bits ---------> |
            eax [11010101 01010101 01010101 01010101]
            ebx [???????? ???????? ???????? ????????]
            ...
    */

    //--- Negacja (uzupełnienie dwójkowe)
    __asm {
        mov eax, p
        neg eax ;zawartość rejestru eax została zanegowana
        mov [wynik], eax
    }
    showVariable(&wynik, "wynik = !p");

    /*
        Operacja negacji odwraca znak liczby we wskazanym rejestrze (0-eax).
        Aby wynik był poprawny najpierw bity muszą zostać odwrócone a następnie
        do wyniku dodana jedynka.

        p =  715827882 DEC => 00101010 10101010 10101010 10101010 BIN
                            ~ _______________________________________
                              11010101 01010101 01010101 01010101 BIN

                              11010101 01010101 01010101 01010101 BIN
                                                                1 BIN
                            + _______________________________________
                              11010101 01010101 01010101 01010110 BIN

        Podgląd na rejestry:
                | <------------- 32 bits ---------> |
            eax [11010101 01010101 01010101 01010110]
            ebx [???????? ???????? ???????? ????????]
            ...
    */

    //--- Przesunięcie bitowe w lewo 
    __asm {
        mov eax, p
        shl eax, 1 ;zawartość rejestru eax została przesunięta w lewo o jeden bit
        mov [wynik], eax
    }
    showVariable(&wynik, "wynik = (p<<1)");

    /*
        p =  715827882 DEC => 00101010 10101010 10101010 10101010 BIN
                       CF<-(0)0101010 10101010 10101010 10101010  BIN
                          << ________________________________________
                              0101010 10101010 10101010 101010100 BIN

        W wyniku przesunięcia bitowego w lewo, bit utracony z lewej strony wędruje do rejestru
        flagowego (do Carry Flag), dzięki temu istnieje możliwość sprawdzenia jaki bit został
        utracony. Gdy zostanie wykonane przesunięcie bitowe o więcej niż jeden bit, CF w rejestrze
        flagowym przyjmie stan zgodny z ostatnim przeniesionym bitem.

        Podgląd na rejestry:
                | <------------- 32 bits ---------> |
            eax [0101010 10101010 10101010 101010100]
            ebx [???????? ???????? ???????? ????????]
            ...

        Operacja przesuwania bitowego w lewo jest jednocześnie mnożeniem liczby przez podstawę
        systemu, czyli mnożeniem przez 2
    */


    //--- Przesunięcie bitowe w prawo 
    __asm {
        mov eax, p
        shr eax, 1 ;zawartość rejestru eax została przesunięta w prawo o jeden bit
        mov [wynik], eax
    }
    showVariable(&wynik, "wynik = (p>>1)");

    /*
        p =  715827882 DEC => 00101010 10101010 10101010 10101010  BIN
                               00101010 10101010 10101010 1010101(0)->CF
                          >> _________________________________________
                              00101010 10101010 10101010 10101010  BIN

        W wyniku przesunięcia bitowego w prawo, bit utracony z prawej strony wędruje do rejestru
        flagowego (do Carry Flag), dzięki temu istnieje możliwość sprawdzenia jaki bit został
        utracony. Gdy zostanie wykonane przesunięcie bitowe o więcej niż jeden bit, CF w rejestrze
        flagowym przyjmie stan zgodny z ostatnim przeniesionym bitem.

        Podgląd na rejestry:
                | <------------- 32 bits ---------> |
            eax [00101010 10101010 10101010 10101010]
            ebx [???????? ???????? ???????? ????????]
            ...

        Operacja przesuwania bitowego w prawo jest jednocześnie dzieleniem liczby przez podstawę
        systemu, czyli dzieleniem przez 2
    */

    //--- Porównanie
    __asm {
        mov eax, p
        mov ebx, q
        cmp eax, ebx
    }
    /*
        Instrukcja cmp do określenia stanu która ze stron jest większa/mniejsza/równa wykorzystuje
        rejestr flagowy, flagi CF(Carry Flag) i ZF(Zero Flag).

        Przyjmując konwencję cel/źródło:
        cmp cel, źródło

        Tablica flag dla instrukcji cmp będzie wyglądała następująco
        ZF  CF
        0   1  gdy cel < źródło
        0   0  gdy cel > źródło
        1   0  gdy cel = źródło

        Instrukcja cmp działa jak zestawienie odejmowania i przesunięcia bitowego w lewo:

        p =  715827882 DEC => 00101010 10101010 10101010 10101010 BIN
        q = 1431655765 DEC => 01010101 01010101 01010101 01010101 BIN
                           - ________________________________________
                              11010101 01010101 01010101 01010101 BIN
                   ZF<-(0) ponieważ wynikiem odejmowania nie było zero

                              11010101 01010101 01010101 01010101 BIN
                          << ________________________________________
                       CF<-(1)10101010 10101010 10101010 10101010 BIN

        ZF  CF
        0   1 oznacza to że cel jest mniejszy niż źródło czyli p < q (715827882 < 1431655765)
    */

    //--- Skok bezwarunkowy 
    __asm {
        jmp toMiejsce1 ;skok bezwarunkowy powoduje przejście procesora do
            ;wykonywania instrukcji z adresu określonego etykietą toMiejsce
            ;...
            mov eax, q ;ta instrukcja się nie wykona
            ;...
            ;...
        toMiejsce1:
            mov eax, p ;ta instrukcja się wykona
            mov [wynik], eax
    }
    showVariable(&wynik, "wynik = p");

    /*
        Deklarując etykietę należy pamiętać o zakończeniu jej znakiem dwukropka ":".
        Używając instrukcji skoku bezwarunkowego (jmp) należy pamiętać aby nie wprowadzić procesora
        w stan nieskończonej pętli, chyba że taki efekt jest zamierzony :)
    */

    //--- Skok warunkowy "gdy wartości równe"
    __asm {
        mov eax, p
        mov ebx, q
        cmp eax, ebx
        je toMiejsce2 ;jeżeli eax i ebx są równe wtedy przeskocz do toMiejsce2
            mov eax, 0 ;w przeciwnym przypadku wykonaj kolejne instrukcje
            mov [wynik], eax
            jmp koniec2; zabezpieczenie przed wykonaniem bloku właściwego dla warunku
        toMiejsce2:
            mov eax, 1
            mov [wynik], eax
        koniec2:
            nop ;ta instrukcja nie robi nic oprócz zużycia cyklu procesora
    }
    showVariable(&wynik, "je");

    /*
        Instrukcje skoku warunkowego opierają się głównie o sprawdzenie odpowiednich flag rejestru
        flagowego. Skok warunkowy można porównać do asemblerowej implementacji warunku 
        w języku C/C++, z tym że bloki kodu dla logicznej prawdy/fałszu są zamienione miejscami.

        C/C++           |   Assembler
        ________________________________
        ...             |   cmp eax, ebx
        if(p==q)        |   je toMiejsce
        {               |
           //...        |      ;...
           //blok kodu  |      ;blok kodu
           //dla prawdy |      ;dla fałszu
           //logicznej  |      ;logicznego
           //...        |      jmp koniec2
                        |
        }else{          |   toMiejsce2:
                        |
           //...        |      ;...
           //blok kodu  |      ;blok kodu
           //dla fałszu |      ;dla prawdy
           //logicznego |      ;logicznej
           //...        |      ;...
        }               |   koniec:

    */

    //--- Skok warunkowy "gdy wartości nierówne"
    __asm {
        mov eax, p
        mov ebx, q
        cmp eax, ebx
        jne toMiejsce3 ;jeżeli eax i ebx nie są równe wtedy przeskocz do toMiejsce3
            mov eax, 0
            mov [wynik], eax
            jmp koniec3
        toMiejsce3:
            mov eax, 1
            mov [wynik], eax
        koniec3:
            nop
    }
    showVariable(&wynik, "jne");

    //--- Skok warunkowy "gdy ostatnia operacja równa zero"
    __asm {
        mov eax, p
        mov ebx, q
        sub eax, ebx
        jz toMiejsce4 ;jeżeli wynik ostatniej operacji wyniósł zero wtedy przeskocz do toMiejsce4
            mov eax, 0
            mov [wynik], eax
            jmp koniec4
        toMiejsce4:
            mov eax, 1
            mov [wynik], eax
        koniec4:
            nop
    }
    showVariable(&wynik, "jz");

    //--- Skok warunkowy "gdy wartość po prawej większa"
    __asm {
        mov eax, p
        mov ebx, q
        cmp eax, ebx
        jg toMiejsce5 ;jeżeli eax jest większe od ebx wtedy przeskocz do toMiejsce5
            mov eax, 0
            mov [wynik], eax
            jmp koniec5
        toMiejsce5:
            mov eax, 1
            mov [wynik], eax
        koniec5:
            nop
    }
    showVariable(&wynik, "jg");

    //--- Skok warunkowy "gdy wartość po prawej większa równa"
    __asm {
        mov eax, p
        mov ebx, q
        cmp eax, ebx
        jge toMiejsce6 ;jeżeli eax jest większe lub równe od ebx wtedy przeskocz do toMiejsce6
            mov eax, 0
            mov [wynik], eax
            jmp koniec6
        toMiejsce6:
            mov eax, 1
            mov [wynik], eax
        koniec6:
           nop
    }
    showVariable(&wynik, "jge");

    //--- Skok warunkowy "gdy wartość po prawej mniejsza"
    __asm {
        mov eax, p
        mov ebx, q
        cmp eax, ebx
        jl toMiejsce7 ;jeżeli eax jest mniejsze od ebx wtedy przeskocz do toMiejsce7
            mov eax, 0
            mov [wynik], eax
            jmp koniec7
        toMiejsce7:
            mov eax, 1
            mov [wynik], eax
        koniec7 :
            nop
    }
    showVariable(&wynik, "jl");

    //--- Skok warunkowy "gdy wartość po prawej mniejsza równa"
    __asm {
        mov eax, p
        mov ebx, q
        cmp eax, ebx
        jle toMiejsce8 ;jeżeli eax jest mniejsze równe od ebx wtedy przeskocz do toMiejsce8
           mov eax, 0
           mov [wynik], eax
           jmp koniec8
        toMiejsce8:
            mov eax, 1
            mov [wynik], eax
        koniec8:
            nop
    }
    showVariable(&wynik, "jl");

    //--- Wywołanie podprogramu i powrót
    __asm {
        mov eax, p
        mov ebx, q
        call podprogram ;wywołanie procedury podprogram

        cmp eax, ebx
        jl koniec

        call podprogram2 ;wywołanie procedury podprogram
        jmp koniec

        podprogram: ;początek procedury podprogram
            add eax, ebx
        ret ;powrót z procedury podprogram

        podprogram2: ;początek procedury podprogram2
            sub eax, ebx
        ret ;powrót z procedury podprogram2

        koniec:
            mov [wynik], eax
    }
    showVariable(&wynik, "call i ret");

    /*
        Instrukcja call i ret służąca kolejno do wywoływania podprogramu i powrotu z niego
        wymaga odłożenia na stos adresu kolejnej instrukcji po call.

        ====================================================================================
        [LINIA]  ADRES: KOD MASZYNOWY    ŹRÓDŁO
        ====================================================================================
        [   1]    0000: B8 0A 00         mov ax, 10
        [   2]    0003: BB 09 00         mov bx, 9
        [   3]    0006: E8 09 00         call podprogram; wywołanie procedury podprogram
        [   4]    0009: 3B C3            cmp ax, bx
        [   5]    000B: 7C 0B            jl koniec
        [   6]    000D: E8 05 00         call podprogram2; wywołanie procedury podprogram
        [   7]    0010: EB 06            jmp koniec
        [   8]        :
        [   9]    0012:                  podprogram: ;początek procedury podprogram
        [  10]    0012: 03 C3                add ax, bx
        [  11]    0014: C3               ret ;powrót z procedury podprogram
        [  12]        :
        [  13]    0015:                  podprogram2: ;początek procedury podprogram2
        [  14]    0015: 2B C3               sub ax, bx
        [  15]    0017: C3               ret;powrót z procedury podprogram2
        [  16]        :
        [  17]    0018:                  koniec:
        [  18]    0018: 8B C8                 mov cx,ax
        [  19]    001A: F4               hlt
        ====================================================================================

        Po wykonaniu linii 3, adres instrukcji z linii 4 (0009) wędruje na stos, następnie
        wskaźnik instrukcji (IP) jest ustawiany na 0012, czyli instrukcję z linii 10.
        Procesor wykonuje instrukcje w tym bloku kodu.
        W momencie napotkania polecenia "ret", ze stosu do wskaźnika instrukcji (IP) jest ściągana
        jest wartość adresu który był kolejny do wykonania przed wywołaniem pod procedury.
        Procesor przechodzi do wykonywania instrukcji z pod adresu 0009.

        Zaimplementowane w ten sposób podprogramy są odpowiednikiem procedur w języku C/C++
        void procedura() | call procedura
        {                | ;...
            //...        | ;...
        }                | ret
    */

    //--- Wywołanie procedury zewnętrznej
    __asm {
        call procedura
    }

    /*
        W powyższym przykładzie została wywołana procedura zewnętrzna,
        która nie przyjmuje żadnych parametrów ani żadnych wartości nie zwraca.
    */

    //--- Wywołanie zewnętrznej funkcji bez parametrów i odebranie wyniku
    __asm {
        call funkcja
        mov [wynik], eax
    }
    showVariable(&wynik, "Wywołanie funkcji bez parametrów");

    /*
        W powyższym przykładzie została wywołana zewnętrzna funkcja o nazwie "funkcja",
        która nie przyjmuje żadnych parametrów, natomiast zwraca wartość typu integer.
        Wartość zwracana jest przechowywana dla tego typu danych w rejestrze eax.

        Przykład asemblerowej implementacji funkcji "funkcja", wygenerowanej przez kompilator:
        int funkcja()
        {
            push        ebp
            mov         ebp,esp
            sub         esp,0C0h
            push        ebx
            push        esi
            push        edi
            lea         edi,[ebp-0C0h]
            mov         ecx,30h
            mov         eax,0CCCCCCCCh
            rep stos    dword ptr es:[edi]
            ;//zwracam 5
            ;return 5;
            mov         eax,5
        }
            pop         edi
            pop         esi
            pop         ebx
            mov         esp,ebp
            pop         ebp
            ret
    */

    //--- Wywołanie zewnętrznej funkcji z parametrami, użycie stosu jako metody przekazywania 
    //--- parametrów do funkcji
    p = 1;
    q = 3;
    r = 5;
    __asm {
        push [r] ;dword ptr r
        push [q]
        push [p]
        call suma
        add esp, 12 ;zmiana wskaźnika wierzchołka stosu
        mov [wynik], eax
    }
    showVariable(&wynik, "Funkcja z parametrem + stos");

    /*
        Przykład asemblerowej implementacji funkcji suma, wygenerowanej przez kompilator:
        int suma(int p, int q, int r)
        {
            push        ebp
            mov         ebp,esp
            sub         esp,0C0h
            push        ebx
            push        esi
            push        edi
            lea         edi,[ebp-0C0h]
            mov         ecx,30h
            mov         eax,0CCCCCCCCh
            rep stos    dword ptr es:[edi]
            ;//zwracam sumę p+q+r
            ;return p + q + r;
            mov         eax,dword ptr [p]
            add         eax,dword ptr [q]
            add         eax,dword ptr [r]
        }
            pop         edi
            pop         esi
            pop         ebx
            mov         esp,ebp
            pop         ebp
            ret

        Należy zwrócić uwagę na kolejność przekazywania parametrów do funkcji.

    */

    //--- Przykład z obliczaniem N-tej liczby Fibonacciego
    //Przykład w C/C++
    int N = 30;
    int i = 0, j = 1;
    int k = 0;
    while (k++ <= N) {
        wynik = j;
        j += i;
        i = wynik;
    }
    showVariable(&wynik, "Fibonacci(N) in C/C++");

    //Przykład w assemblerze
    __asm {
        mov ecx, 30 ;N
        mov eax, 1  ;j
        mov ebx, 0  ;i
        start:
        mov edx, eax ;edx odpowiednik tmp
            add eax, ebx
            mov ebx, edx
        loop start
        mov [wynik], eax
    }
    showVariable(&wynik, "Fibonacci(N) in asm");

    //--- Przykład zliczania 1 w rejestrze i skok warunkowy jnc 
    //--- jnc (jump if not carry flag)
    unsigned char a = 255;//11111111
    unsigned char w = 0;
    __asm{
        mov al, a
        mov ah, 0
        ;do implementacji pętli można użyć ecx
        ;następnie je dekrementować i sprawdzać jecxz (jump if ecx is zero)
        petla:

            cmp al, 0
            jz koniec_liczenia

            shl al, 1
            jnc petla
            
            inc ah

        jmp petla

        koniec_liczenia:
            mov [w], ah
    }
    wynik = w;
    showVariable(&wynik, "zliczanie 1");

    //--- Przykład zliczania 1 w rejestrze + loop
    a = 128; //10000000
    __asm {
        mov al, a
        mov ah, 0
        mov ecx, 8

        petla2:

            cmp al, 0
            jz koniec_liczenia2

            shl al, 1
            jnc petla2

            inc ah

        loop petla2
        koniec_liczenia2:
            mov [w], ah
    }
    wynik = w;
    showVariable(&wynik, "zliczanie 1 + loop");

    //--- Odwracanie bajtu kolejnością
    a = 0x82; //10000010
    __asm {
        mov al, a
        mov ah, 0
        mov bl, a
        mov cl, 1
        mov ch, 0x80
        
        petlaZ:
            and bl, cl
            jnz dod1
                mov bl, al
                shr ch, 1
                shl cl, 1
                jz kon
                jmp petlaZ
            dod1:
                or ah, ch
                mov bl, al
                shr ch, 1
                shl cl, 1
                jz kon
        jnz petlaZ
        kon:
            mov [w], ah
    }
    wynik = w;
    showVariable(&wynik, "Obracanie bajtu");

    getchar();
    return 0;
}